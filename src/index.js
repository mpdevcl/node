require('dotenv').config()

const Koa = require('koa')
const KoaRouter = require('koa-router')
const Logger = require('koa-logger') // Para logs en terminal
const bodyParser = require('koa-bodyparser')
const axios = require('axios')
const Movie = require('./models/movie')

const router = new KoaRouter()
const app = new Koa()

app.use(bodyParser())

const db = require('./dbconnect/mongodb')
db.connect()

/**
 * Middleware para manejo de errores
 */
app.use(async (ctx, next) => {
  try {
    await next()
  } catch (err) {
    console.error(err)

    ctx.body = `Ha ocurrido un error<br>:${err.message}`
  }
})

/**
 * La raíz de la API devuelve todas las películas de la base de datos.
 * Los resultados se paginan de 5 en 5.
 */
router.get('/', async ctx => {
  let paged = 1
  const limit = 5

  if (ctx.get('page')) {
    paged = ctx.get('page')
  }

  const response = await Movie.paginate({}, {page: paged, limit: limit})

  if (response.docs.length == 0) {
    ctx.status = 404
    ctx.body = 'No se han encontrado resultados'
  } else {
    ctx.body = response.docs
  }
})

/**
 * Endpoint para buscar películas.
 * Cuando la película no está en la base de datos, se crea con
 * la información de la API.
 */
router.get('/search', async ctx => {
  const searchTerm = ctx.query.s
  const searchYear = ctx.query.y
  const regex = new RegExp(searchTerm, 'i') // La búsqueda será case insensitive
  const response = await Movie.find({ title: { $regex: regex } })

  if (searchTerm && response.length == 0) {
    const resAPI = await axios.get(`${process.env.API_URL}?t=${searchTerm}&y=${searchYear}&type=movie&apikey=${process.env.API_KEY}`)

    if (resAPI.data.Response == 'True') {
      const info = {
        title: resAPI.data.Title,
        year: resAPI.data.Year,
        released: resAPI.data.Released,
        genre: resAPI.data.Genre,
        director: resAPI.data.Director,
        actors: resAPI.data.Actors,
        plot: resAPI.data.Plot,
        ratings: resAPI.data.Ratings,
      }

      const nuevoItem = new Movie(info)
      const save = await nuevoItem.save()

      ctx.body = save
    } else {
      ctx.body = 'No se encontró la película'
    }
  } else {
    ctx.body = response
  }
})

/**
 * Endpoint para buscar y reemplazar.
 * El valor de "plot" actualizado se muestra en la respuesta.
 */
router.post('/replace', async ctx => {
  const movieName = new RegExp(ctx.request.body.movie, 'i')
  const find = new RegExp(ctx.request.body.find, 'i')
  const response = await Movie.findOne({ title: { $regex: movieName }})

  if (!response) {
    ctx.status = 404
    ctx.body = 'No se encontraron resultados'
  } else {
    ctx.body = response.plot.replace(find, ctx.request.body.replace)
  }
})

app
  .use(Logger())
  .use(router.routes())
  .use(router.allowedMethods())

app.listen(3000, () => {
  console.log('Servidor OK')
})
