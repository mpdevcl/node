const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate-v2')
const Schema = mongoose.Schema

const movieSchema = new Schema({
  title: String,
  year: String,
  released: String,
  genre: String,
  director: String,
  actors: String,
  plot: String,
  ratings: Array,
})

movieSchema.plugin(mongoosePaginate)

const Movies = mongoose.model('movies', movieSchema)

module.exports = Movies
