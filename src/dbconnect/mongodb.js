const mongoose = require('mongoose');

module.exports = class DB {
  static connect() {
    return mongoose.connect('mongodb://mongo/movies', {
      promiseLibrary: true,
      useNewUrlParser: true,
      useUnifiedTopology: true,
    });
  }
}
