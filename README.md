# Node API

## Instalación
### Con Docker
Clonar el repositorio y ejecutar `docker-compose up`

### Sin Docker
Es necesario contar con Node y MongoDB instalados en el sistema.

1. Una vez clonado el repositorio, instalar las dependencias con `npm install`.
2. Editar el archivo `src/dbconnect/mongodb.js` con el host de Mongo correspondiente al sistema.
3. Ejecutar `npm start` para levantar el servidor.

## Funcionamiento
La API consta de 3 endpoints para realizar operaciones en la base de datos de MongoDB.
Tras levantar el servidor, la aplicación quedará accesible desde el puerto 3000 del localhost. Ejemplo: `http://localhost:3000`.

### GET /
La raíz de la API devuelve todas las películas almacenadas en la base de datos. Los resultados son paginados mostrando 5 resultados por página.
El número de página va definido en el valor `page` del header.

### GET /search
El endpoint "search" recibe dos parámetros en la URL:

|Parámetro|Tipo|Descripción|
|-|-|-|
|s|Requerido|Corresponde al término a buscar en la base de datos. Ejemplo: "star wars"|
|y|Opcional|Corresponde al año de la película|

Se busca el término ingresado en la base de datos y de no existir una coincidencia, se realiza una consulta a la API externa, almacenando el valor en la base de datos.

### POST /replace
Este endpoint recibe un objeto en el body de la petición y realiza una operación de reemplazo según la información enviada.

El objeto esperado en el body tiene el siguiente formato:
```
{
  "movie": "star wars",
  "find": "jedi",
  "replace": "CLM Dev"
}
```

Se retornará una cadena con el campo "plot" modificado, reemplazando el término definido en "find" por el de "replace".
